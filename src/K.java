import java.util.Objects;

public class K<K> {

   private K key;

    public K(K key) {
        this.key = key;
    }


    @Override
    public String toString() {
        return "Value " + key ;
    }

}
